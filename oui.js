document.addEventListener('DOMContentLoaded', function()
{
  var suite = document.getElementById('suite'),
    choix = document.getElementById('choix'),
    error = document.getElementById('error'),
    base,
    nb,
    bool,
    suivante;

  document.addEventListener('keydown', function(event)
  {
    if(event.keyCode == 13)
    {
      event.preventDefault();
      let oui = parseInt(choix.innerText);
      if(oui > 1)
      {
        creaSuite(oui);
      }
      else
      {
        error.innerText = 'Il faut rentrer un nombre entier supérieur à 1 (en chiffres évidemment)'
      }
    }
  });

  function creaSuite(x)
  {
    error.innerText = '';
    let j = suite.children.length
    for(i = 1; i < j; i++)
    {
      suite.removeChild(suite.children[1]);
    }
    for(i = 0; i < x-1; i++)
    {
      base = suite.children[i].innerText;
      suivante = '';
      for(j = 0; j < base.length; j++)
      {
        let char = base.charAt(j);
        nb = 1;
        while (char == base.charAt(j+1) && j < base.length)
        {
          bool = true;
          nb++;
          j++;
        }
        suivante += nb + '' + char;
      }
      let next = document.createElement('p');
      next.innerText = suivante;
      suite.appendChild(next);
      let a = 0;
    }
  }
});
